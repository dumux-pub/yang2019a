Summary
=======
This is the DuMuX module containing the code for producing the REV-based multi-domain modelling results presented in:

G. Yang, E. Coltman, K. Weishaupt, A. Terzis, R. Helmig, B. Weigand<br>
[On the Beavers-Joseph interface condition for non-parallel coupled channel flow over a porous structure at high Reynolds numbers]()<br>
Transport In Porous Media, January 2019.<br>

Installation
============
The easiest way to install this module is to execute the file
[installYang2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Yang2019a/raw/master/installYang2019a.sh)
in your working space.

```bash
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Yang2019a/raw/master/installYang2019a.sh
sh ./installYang2019a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.

Folder Structure
================
This dumux module has the following structure.
The *appl* folder contains the problem specific source code for the presented results

Run Simulations
===============
To run simulations type e.g.:
```bash
cd Yang2019a/build-cmake/appl/
make test_md_boundary_darcy1pforchheimer_rans1p
./test_md_boundary_darcy1pforchheimer_rans1p params_horizontalflow.input
./test_md_boundary_darcy1pforchheimer_rans1p params_convergingflow.input
```

Used Versions and Software
==========================
For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installYang2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Yang2019a/raw/master/installYang2019a.sh).
This publication bases on dumux/3.0 but is attached to a specific branch.<br>
In addition, external software packages are necessary for compiling the executables and has been tested
for the following versions:

| software           | version | type              |
| ------------------ | ------- | ----------------- |
| cmake              | 3.5.2   | build tool        |
| clang/clang++      | 3.5.0   | compiler          |
| gcc/g++            | 6.2.1   | compiler          |
| UMFPack            | 5.7.1   | linear solver     |
| SuperLU            | 4.3     | linear solver     |
| suitesparse        | 4.5.5   | matrix algorithms |


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Yang2019a
cd Yang2019a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/yang2019a/-/raw/master/docker_yang2019a.sh
```

Open the Docker Container
```bash
bash docker_yang2019a.sh open
```

After the script has run successfully, one may build the executable

```bash
cd yang2019a/build-cmake/appl/
make test_md_boundary_darcy1pforchheimer_rans1p
```

and it can be executed with an input file e.g., by running

```bash
./test_md_boundary_darcy1pforchheimer_rans1p params_horizontalflow.input
./test_md_boundary_darcy1pforchheimer_rans1p params_convergingflow.input
```
